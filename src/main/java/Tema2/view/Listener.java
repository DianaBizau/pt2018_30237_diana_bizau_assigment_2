package Tema2.view;

import Tema2.model.Client;
import Tema2.model.Coada;


import java.util.Random;

public class Listener
{
    private Interfata view;
    private Coada[] cozi = new Coada[MAX_QUEUES];
    private Client[] clienti = new Client[MAX_CLIENTS];
    private double[] rushHour;
    private int noQueues;
    private int noClients;
    private int currentHour;
    private int finalHour;
    private int minutesPassed;
    private int currentClient;
    Interfata inte=new Interfata();


    private static int MAX_QUEUES = Interfata.getQueues();
    static int MAX_CLIENTS = Interfata.getClients();
    private int CLIENTS_DELAY;

    public Listener()
    {
    }
    public void setControler(Interfata view)
    {
        this.view = view;
        this.currentHour = view.getSimulationBegining();
        this.finalHour = view.getSimulationEnding();
        this.minutesPassed = 0;
        currentClient = 0;
        noQueues = view.getNumberQueues();
        noClients = view.getNumberClients();
        ajunsClient();
        CLIENTS_DELAY = view.getDefaultDelay();
        Map();
    }

    public void Map()
    {
        int i,j;
        int randomTime;
        int lastRandom;
        int randomClients;
        int servingTime;
        int minServingTime = view.getMinServingTime();
        int maxServingTime = view.getMaxServingTime();

        for (i = 0; i < noQueues; i++)
        {
            cozi[i] = new Coada(i);
        }

        minutesPassed = 0;

        Random r = new Random();
        lastRandom = currentHour*60;
        currentClient = 0;
        while(currentClient<noClients)
        {
            randomTime = (int) (1 + r.nextInt(CLIENTS_DELAY) * rushHour[currentHour]);
            minutesPassed += randomTime;

            if(minutesPassed >= 60)
            {
                currentHour++;
                if(currentHour>=24)
                {
                    currentHour=24;
                }
                minutesPassed -= 60;
            }

            randomTime += lastRandom;
            lastRandom = randomTime;
            randomClients = 1+r.nextInt(view.getNumberConcurentClients());
            //System.out.println("randomClients = " + randomClients);

            for(j = 0; (currentClient < noClients)&&(j < randomClients); j++)
            {
                currentClient++;
                servingTime = minServingTime + r.nextInt(maxServingTime - minServingTime);
                clienti[currentClient] = new Client(randomTime,servingTime,currentClient);
                view.addLog("Se genereaza clienti : " + clienti[currentClient].toStringArrival());
            }
        }

        currentHour = view.getSimulationBegining();
        minutesPassed = 0;
        currentClient = 1;

    }
    //vector pt aparitia clientilor
    private void ajunsClient()
    {
        rushHour = new double[]{/*0*/2,2,2,2,2,2,/*6*/1.5,1,1,1,1,/*11*/0.75,0.75,0.6,0.5,/*15*/0.3,0.5,0.6,0.3,/*19*/0.4,0.6,0.7,1,2,2};
    }

    public boolean makeAction()
    {
        int currentTime;
        int addQue;
        int i;
        int exit;

        minutesPassed++;
        if (minutesPassed >= 60)
        {
            minutesPassed = 0;
            currentHour++;
        }

        if ((currentHour >= finalHour))
        {
            return false;
        }
        else
        {
            currentTime = currentHour*60 + minutesPassed;
            exit = 0;
            while((currentClient <= noClients)&&(exit == 0))
            {
                if (clienti[currentClient].getArrivalTime()<=currentTime)
                {
                    addQue = getMinimalWaitingTimeQue();
                    cozi[addQue].addClient(clienti[currentClient]);
                    view.addLog("Se adauga " + clienti[currentClient].toString() + " la casa "+ addQue + " la timpul " + currentHour + ":" + minutesPassed);

                    if(addQue==0)
                    {
                        view.addImage();

                    }
                    else if(addQue==1)
                    {
                        view.addImage1("*");
                    }
                    else if(addQue==2)
                    {
                        view.addImage2("*");
                    }
                    else if(addQue==3)
                    {
                        view.addImage3("*");
                    }
                    else if(addQue==4)
                    {
                        view.addImage4();
                    }
                    currentClient++;
                }
                else
                {
                    exit = 1;
                }
            }
            view.setTextArea();
            for (i = 0; i < noQueues; i++)
            {
                cozi[i].servClient();
                view.addTextArea("" + cozi[i].toString()+ "\n");
            }
        }
        view.addTextArea("Current Time: " + currentHour + ":" + minutesPassed + "\n");
        view.addTextArea("\n");
        for (i = 0; i < noQueues; i++)
        {
            if (cozi[i].getCurrentServingTime()==0)
            {
                Client ca = cozi[i].getCurrentClient();
                addQue = getMinimalWaitingTimeQue();
                view.addTextArea("Clientul " + ca.getClientID() + " a asteptat " + ca.getWaitingTime() + " din " + ca.getServingTime() + "\n");
                view.addLog("Paraseste " + ca.toStringWaiting() + " de la casa " + i);
            }
        }
        return true;
    }
    private int getMinimalWaitingTimeQue()
    {
        int minQue = 0;
        int minWaitTime = cozi[0].timpAsteptare();
        int currentWaitTime;

        for(int i = 1; i < noQueues; i++ )
        {
            currentWaitTime = cozi[i].timpAsteptare();
            if (currentWaitTime < minWaitTime)
            {
                minWaitTime = currentWaitTime;
                minQue = i;
            }
        }
        return minQue;
    }
}
