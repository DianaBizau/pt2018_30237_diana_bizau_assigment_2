package Tema2.view;

import Tema2.controller.MyTimer;


import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;





import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;



public class Interfata
{
    private MyTimer timerTask;
    private Listener controler;
    private int paused;

    private static final int SET_CONCURENT_CLIENTS = 1;
    private static final int SET_QUEUES = 3;
    private static final int SET_CLIENTS = 12;
    private static final int MAX_SERVING_TIME = 15;
    private static final int MIN_SERVING_TIME = 5;
    private static final int DEFAULT_DELAY = 10;

    private JTextArea writeResultsTextArea;
    private JTextArea logTextArea;

    private static final int MAX_QUEUES = 8;

    private static final int MAX_CLIENTS = 501;

    private static JFrame window = new JFrame("Aplicatie Supermarket");
    private static JPanel menu = new JPanel();
    private static JPanel panel1 = new JPanel();
    private static JPanel panel2 = new JPanel();
    private static JPanel panel3 = new JPanel();
    private static JTextField t = new JTextField();
    private static JTextField t1 = new JTextField();
    private static JTextField t2 = new JTextField();
    private static JTextField t3 = new JTextField();
    private static JTextField t4 = new JTextField();
    private static JTextField t5 = new JTextField();
    private static JTextField t6 = new JTextField();
    private static JTextField coadatext = new JTextField();
    public static JButton start = new JButton("<html> <font size = 3 color = green>Start </font> </html>");
    public static JButton pause = new JButton("<html> <font size = 3 color = green>Pause </font> </html>");
    JTextArea c6=new JTextArea();
    JTextArea c7=new JTextArea();
    JTextPane c1=new JTextPane();
    JTextPane c2=new JTextPane();
    JTextPane c3=new JTextPane();
    JTextPane c4=new JTextPane();
    JTextPane c5=new JTextPane();
    JTextArea x=new JTextArea();

    public Interfata()
    {
        timerTask = new MyTimer(this);
        paused = 0;
        this.init();
    }
    public void init()
    {
        window.getContentPane().add(menu);
        window.getContentPane().add(panel1);
        window.setSize(1300, 580);
        window.setLocation(40,0);
        window.setLayout(null);
        window.getContentPane();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.revalidate();
        window.setVisible(true);

        panel1.setLayout(null);
        panel1.setBorder(BorderFactory.createMatteBorder(2,4,2,2,Color.black));
        panel1.setBounds(600, 0, 700, 532);
        panel1.setBackground(Color.white);
        panel1.setVisible(true);


        c1.setBounds(200, 50, 300, 50);
        c1.setBorder(BorderFactory.createMatteBorder(0,0,2,2,Color.black));
        panel1.add(c1);

        c2.setBounds(200, 150, 300, 50);
        c2.setBorder(BorderFactory.createMatteBorder(0,0,2,2,Color.black));
        panel1.add(c2);

        c3.setBounds(200, 250, 300, 50);
        c3.setBorder(BorderFactory.createMatteBorder(0,0,2,2,Color.black));
        panel1.add(c3);

        c4.setBounds(200, 350, 300, 50);
        c4.setBorder(BorderFactory.createMatteBorder(0,0,2,2,Color.black));
        panel1.add(c4);

        c5.setBounds(200, 450, 300, 50);
        c5.setBorder(BorderFactory.createMatteBorder(0,0,2,2,Color.black));
        panel1.add(c5);

        JLabel title2 = new JLabel("CASA 1", JLabel.RIGHT);
        title2.setForeground(Color.BLACK);
        title2.setFont(new Font("Arial", Font.BOLD, 10));
        title2.setBounds(200, 10, 50, 30);
        panel1.add(title2);
        JLabel title3 = new JLabel("CASA 2", JLabel.RIGHT);
        title3.setForeground(Color.BLACK);
        title3.setFont(new Font("Arial", Font.BOLD, 10));
        title3.setBounds(200, 110, 50, 30);
        panel1.add(title3);
        JLabel title4 = new JLabel("CASA 3", JLabel.RIGHT);
        title4.setForeground(Color.BLACK);
        title4.setFont(new Font("Arial", Font.BOLD, 10));
        title4.setBounds(200, 210, 50, 30);
        panel1.add(title4);
        JLabel title5 = new JLabel("CASA 4", JLabel.RIGHT);
        title5.setForeground(Color.BLACK);
        title5.setFont(new Font("Arial", Font.BOLD, 10));
        title5.setBounds(200, 310, 50, 30);
        panel1.add(title5);
        JLabel title6 = new JLabel("CASA 5", JLabel.RIGHT);
        title6.setForeground(Color.BLACK);
        title6.setFont(new Font("Arial", Font.BOLD, 10));
        title6.setBounds(200, 410, 50, 30);
        panel1.add(title6);

        panel2.setLayout(null);
        panel2.setBorder(BorderFactory.createMatteBorder(2,2,2,2,Color.black));
        panel2.setBounds(0, 0, 701, 370);
        panel2.setBackground(Color.white);
        panel2.setVisible(true);
        JLabel title7 = new JLabel("REZULTATE", JLabel.LEFT);
        title7.setForeground(Color.GREEN);
        title7.setFont(new Font("Arial", Font.BOLD, 12));
        title7.setBounds(20, 0, 100, 30);
        panel2.add(title7);

        panel3.setLayout(null);
        panel3.setBorder(BorderFactory.createMatteBorder(2,2,2,2,Color.black));
        panel3.setBounds(0, 370, 600, 162);
        panel3.setBackground(Color.white);
        panel3.setVisible(true);
        JLabel title8 = new JLabel("LOG FILE", JLabel.LEFT);
        title8.setForeground(Color.GREEN);
        title8.setFont(new Font("Arial", Font.BOLD, 12));
        title8.setBounds(20, 0, 100, 30);
        panel3.add(title8);

        writeResultsTextArea = new JTextArea(20, 100);
        writeResultsTextArea.setBounds(20,35,400,150);
        writeResultsTextArea.setLineWrap(true);
        writeResultsTextArea.setWrapStyleWord(true);
        Border border = BorderFactory.createLineBorder(Color.black);
        writeResultsTextArea.setBorder(border);
        writeResultsTextArea.setEditable(false);
        JScrollPane scrollPane1 = new JScrollPane( writeResultsTextArea);
        scrollPane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        panel2.add(scrollPane1);
        scrollPane1.setBounds(20,30,400,330);
        JLabel c13=new JLabel();
        c13.setBounds(450, 0, 100, 370);
        c13.setBorder(BorderFactory.createMatteBorder(0,4,0,0,Color.black));
        panel2.add(c13);
        JLabel c14=new JLabel("Nr.Clienti:");
        c14.setBounds(460, 0, 170, 50);

        panel2.add(c14);
        t2.setBounds(520, 7, 30, 30);
        panel2.add(t2);
        JLabel c15=new JLabel("Nr.Cl.(concurenti):");
        c15.setBounds(460, 20, 170, 50);

        panel2.add(c15);
        t3.setBounds(565, 30, 30, 30);
        panel2.add(t3);
        JLabel c16=new JLabel("Delay:");
        c16.setBounds(460, 50, 170, 50);

        panel2.add(c16);
        t4.setBounds(520, 60, 30, 30);
        panel2.add(t4);
        JLabel c20=new JLabel("Nr.Cozi:");
        c20.setBounds(460, 80, 170, 50);
        panel2.add(c20);
        coadatext.setBounds(520, 90, 30, 30);
        panel2.add(coadatext);
        JLabel c17=new JLabel("Timp servire:");
        c17.setBounds(485, 100, 170, 50);

        panel2.add(c17);
        t5.setBounds(485, 135, 30, 30);
        panel2.add(t5);
        JLabel c18=new JLabel(":");
        c18.setBounds(525, 105, 170, 50);
        panel2.add(c18);
        t6.setBounds(535, 135, 30, 30);
        panel2.add(t6);
        logTextArea = new JTextArea(20, 100);
        logTextArea.setBounds(20,35,400,130);
        logTextArea.setLineWrap(true);
        logTextArea.setWrapStyleWord(true);
        Border border1 = BorderFactory.createLineBorder(Color.black);
        logTextArea.setBorder(border1);
        logTextArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(logTextArea);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        panel3.add(scrollPane);
        scrollPane.setBounds(20,30,400,130);

        JLabel c10=new JLabel();
        c10.setBounds(440, 0, 100, 170);
        c10.setBorder(BorderFactory.createMatteBorder(0,4,0,0,Color.black));
        panel3.add(c10);
        JLabel c11=new JLabel("  TIME:");
        c11.setBounds(440, 0, 170, 50);
        c11.setBorder(BorderFactory.createMatteBorder(0,0,4,0,Color.black));
        panel3.add(c11);
        t.setBounds(490, 7, 30, 30);
        panel3.add(t);
        JLabel c12=new JLabel(":");
        c12.setBounds(530, 0, 170, 50);
        c12.setFont(new Font("Arial", Font.BOLD, 12));
        panel3.add(c12);
        t1.setBounds(540, 7, 30, 30);
        panel3.add(t1);
        start.setBounds(480, 60, 85, 30);

        pause.setBounds(480,100,85,30);
        panel3.add(start);
        panel3.add(pause);
        window.getContentPane().add(panel3);
        window.getContentPane().add(panel2);
        window.getContentPane().add(menu);

        t = new JTextField(""+13);
        t1 = new JTextField(""+14);
        t3 = new JTextField(""+SET_CONCURENT_CLIENTS);
        coadatext= new JTextField(""+SET_QUEUES);
        t2 = new JTextField(""+SET_CLIENTS);
        t5 = new JTextField(""+MIN_SERVING_TIME);
        t6 = new JTextField(""+MAX_SERVING_TIME);
        t4 = new JTextField(""+DEFAULT_DELAY);

        window.validate();
        window.setVisible(true);

        start.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                startActionHandler();}
        });

        pause.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                pauseActionHandler();
            }
        });

    }

    void startActionHandler()
    {
        controler = new Listener();
        controler.setControler(this);
        Timer timer = new Timer();
        timerTask.setRunning();
        timerTask = new MyTimer(this);
        timer.schedule(timerTask, 0, 1000);
    }

    void pauseActionHandler()
    {
        if(paused == 0)
        {
            paused = 1;
            pause.setText("Resume");
        }
        else {
            paused = 0;
            pause.setText("Pause");
        }
    }

    public static int getClients()
    {
        return MAX_CLIENTS;
    }

    public static int getQueues()
    {
        return MAX_QUEUES;
    }

    public int getSimulationBegining()
    {
        return Integer.parseInt(t.getText());
    }

    public int getSimulationEnding()
    {
        return Integer.parseInt(t1.getText());
    }

    public int getNumberClients()
    {
        return Integer.parseInt(t2.getText());
    }

    public MyTimer getTimer()
    {
        return timerTask;
    }
    public int getNumberQueues()
    {
        return Integer.parseInt(coadatext.getText());
    }
    public int getNumberConcurentClients()
    {
        return  Integer.parseInt(t3.getText());
    }

    public void setControler(Listener c)
    {
        controler = c;
    }

    public Listener getControler()
    {
        return controler;
    }

    public int getPause()
    {
        return paused;
    }

    public void setTextArea()
    {
        writeResultsTextArea.setText(" ");
    }

    public void addTextArea(String s)
    {
        writeResultsTextArea.append(s+"\n");
    }

    public int getMinServingTime()
    {
        return Integer.parseInt(t5.getText());
    }

    public int getMaxServingTime()
    {
        return Integer.parseInt(t6.getText());
    }

    public int getDefaultDelay()
    {
        return Integer.parseInt(t4.getText());
    }

    public void addLog(String s)
    {
        logTextArea.append(s+"\n");
    }
    public void addImage()
    {
        ImageIcon imgThisImg = new ImageIcon("people.gif");
        c1.insertIcon(imgThisImg);
    }
    public void addImage1(String m){
        ImageIcon imgThisImg = new ImageIcon("people.gif");
        c2.insertIcon(imgThisImg);
    }
    public void addImage2(String m)
    {
        ImageIcon imgThisImg = new ImageIcon("people.gif");
        c3.insertIcon(imgThisImg);
    }
    public void addImage3(String m)
    {
        ImageIcon imgThisImg = new ImageIcon("people.gif");
        c4.insertIcon(imgThisImg);
    }
    public void addImage4()
    {
        ImageIcon imgThisImg = new ImageIcon("people.gif");
        c5.insertIcon(imgThisImg);
    }
}



