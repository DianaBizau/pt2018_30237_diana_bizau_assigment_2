package Tema2;

import Tema2.view.Interfata;
import Tema2.view.Listener;


import javax.swing.SwingUtilities;

public class Test
{
    private Listener ctrl;
    private Interfata gui;

    public Test()
    {
        gui = new Interfata();
        ctrl = new Listener();
        gui.setControler(ctrl);
    }

    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                // TODO Auto-generated method stub
                new Test();
            }
        });
    }
}
