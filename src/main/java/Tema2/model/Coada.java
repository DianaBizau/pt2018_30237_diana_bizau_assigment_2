package Tema2.model;

import java.util.ArrayList;

public class Coada {
    private ArrayList<Client> persoane;
    private int currentServingTime;
    private int start = 0;
    private int coadaID;

    public Coada(int coadaID)
    {
        persoane = new ArrayList<Client>(10);
        currentServingTime = 0;
        this.coadaID = coadaID;
    }

    public int timpAsteptare()
    {
        int inter = currentServingTime;
        for(int i = 1; i < persoane.size(); i++)
        {
            inter += persoane.get(i).getServingTime();
        }
        return inter;
    }
    public Client getCurrentClient()
    {
        return  persoane.get(start);
    }
    public int getCurrentServingTime()
    {
        if(persoane.isEmpty())
        {
            return -1;
        }
        return currentServingTime;
    }
    public void addClient(Client client)
    {
        if(persoane.isEmpty())
        {
            currentServingTime = client.getServingTime();
        }
        persoane.add(client);
    }
    public Client scoateClient()
    {
        return persoane.remove(start);
    }
    public void servClient()
    {
        if (persoane.isEmpty())
        {
        }
        else{
            currentServingTime--;
            for(int i = 0; i < persoane.size(); i++)
            {
                persoane.get(i).cresteWaitingTime();
            }
            if(currentServingTime<0)
            {
                this.scoateClient();
                if (!persoane.isEmpty())
                {
                    currentServingTime = this.getCurrentClient().getServingTime();
                }
            }
        }

    }

    public String toString()
    {
        String sir="Casa "+ coadaID + " : ";
        if (!persoane.isEmpty())
        {
            sir = sir + "Clientul "+persoane.get(0).getClientID() + " cu timpul ramas de servire " + currentServingTime+", ";
        }

        for (int i = 1; i < persoane.size(); i++)
        {
            sir = sir + persoane.get(i).toString()+", ";
        }
        return sir;
    }

}
