package Tema2.model;

public class Client {
    private int arrivalTime;
    private int servingTime;
    private int waitingTime;
    private int clientID;

    private static final int max_time = 24*60;

    public Client(int arrival, int serving, int clientID)
    {
        if (arrival <= max_time)
        {
            this.arrivalTime = arrival;
        }
        else
        {
            this.arrivalTime = max_time;
        }
        this.servingTime = serving;
        this.clientID = clientID;
        this.waitingTime = 0;
    }
    public int getArrivalTime()
    {
        return arrivalTime;
    }
    public int getServingTime()
    {
        return servingTime;
    }
    public int getWaitingTime()
    {
        return waitingTime;
    }
    public int getClientID()
    {
        return clientID;
    }
    public String toStringArrival()
    {
        String sir="";
        sir = sir + "Clientul "+clientID + " cu timpul de servire " + servingTime +" min "+ " si timpul de sosire " + arrivalTime+" min";
        return sir;
    }

    public String toStringWaiting()
    {
        String sir="";
        sir = sir + "Clientul "+clientID + " cu timpul de servire " + servingTime +" min "+ " si timpul de asteptare " + waitingTime+" min";
        return sir;
    }
    public String toString()
    {
        String sir="";
        sir = sir +"Clientul "+ clientID + " cu timpul de servire " + servingTime+" min";
        return sir;
    }
    public void cresteWaitingTime()
    {
        waitingTime++;
    }

}
