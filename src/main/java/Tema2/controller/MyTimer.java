package Tema2.controller;

import Tema2.view.Interfata;


import java.util.TimerTask;

public class MyTimer extends TimerTask
{
    //times member represent calling times.
    private int running;
    private Interfata view;

    /**
     * constructorul clasei controller.MyTimer
     * @param g interfata ce construieste timerul
     */
    public MyTimer(Interfata g)
    {
        running = 1;
        view = g;
    }
    /**
     * metoda ce defineste functionalitatea pentru fiecare actiune a timerului
     * la fiecare pas testeaza daca este pauzata executia sau nu
     * daca nu este pauzata, atunci apeleaza metoda de makeAction din controler
     * se executa pana cand se dezactiveaza timerul (executia) curent sau pana cand inca se poate desfasura metoda de makeAction
     */
    @Override
    public void run()
    {

        if(view.getPause()==0)
        {
            if (running==1)
            {
                if (view.getControler().makeAction()==false)
                {
                    System.out.println("Timer stops now...");
                    this.cancel();
                }
            }
            else
            {
                System.out.println("Timer stops now");
                this.cancel();
            }
        }
    }

    /**
     * seteaza daca timerul se afla in executie sau nu
     * timerul se termina cand incepe executia unei alte simulari
     */
    public void setRunning()
    {
        running = 0;
    }
}
